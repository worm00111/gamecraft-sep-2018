﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject winText;
    [SerializeField] private GameObject loseText;
    [SerializeField] private GameObject restartText;
    [SerializeField] private GameObject gameOverPanel;

    public void Win()
    {
        winText.SetActive(true);
        restartText.SetActive(true);
        gameOverPanel.SetActive(true);
    }

    public void Lose()
    {
        loseText.SetActive(true);
        restartText.SetActive(true);
        gameOverPanel.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Restart"))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }
}