﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    [SerializeField] private float changeInRotationSpeed;
    [SerializeField] private float rotationSpeed;

    private MovementScript movementScript;
    private CharacterController charController;
    [SerializeField] private float aggroRange = 1;
    [SerializeField] private float dashRange = 1;

    private float RotationSeed;
    private bool aggro;

    // Use this for initialization
    void Start()
    {
        RotationSeed = Random.Range(-100000,100000);
        movementScript = GetComponent<MovementScript>();
        charController = GetComponent<CharacterController>();

        aggroRange *= charController.sizeScalar;
        dashRange *= charController.sizeScalar;
    }

    void FixedUpdate()
    {
        GameObject player = GameObject.FindWithTag("Player");

        CheckForAggro(player);

        if (!aggro)
        {
            float extraAngle = Mathf.PerlinNoise((RotationSeed + Time.time) * changeInRotationSpeed, 0) - 0.5f;
            extraAngle *= rotationSpeed;
            movementScript.RotateFish(extraAngle);

            float horizontal = transform.right.x;
            float vertical = transform.right.y;
            movementScript.MoveFish(horizontal, vertical);
        }
    }

    private void CheckForAggro(GameObject player)
    {
        if (player == null) return;

        float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
        float playerSizeScalar = player.GetComponent<CharacterController>().sizeScalar;

        if (distanceToPlayer < aggroRange && playerSizeScalar < charController.sizeScalar)
        {
            aggro = true;

            Vector3 rotVector = player.transform.position - transform.position;
            rotVector = Vector3.Normalize(rotVector);

            movementScript.RotateFish(rotVector.x, rotVector.y);
            float horizontal = transform.right.x;
            float vertical = transform.right.y;
            movementScript.MoveFish(horizontal, vertical);

            if (distanceToPlayer < dashRange)
            {
                charController.Dash();
            }
        }
        else
        {
            aggro = false;
        }
    }
}