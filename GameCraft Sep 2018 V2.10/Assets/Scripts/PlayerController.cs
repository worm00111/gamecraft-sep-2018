﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterController _characterScript;
    [SerializeField] private MovementScript _movementScriptScript;

    void Update()
    {
        if (Input.GetButtonDown("Dash"))
        {
            _characterScript.Dash();
        }

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (Mathf.Abs(horizontal) > 0 || Mathf.Abs(vertical) > 0)
        {
            _movementScriptScript.RotateFish(horizontal, vertical);
        }
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (Mathf.Abs(horizontal) > 0 || Mathf.Abs(vertical) > 0)
        {
            _movementScriptScript.MoveFish(horizontal, vertical);
        }
    }
}