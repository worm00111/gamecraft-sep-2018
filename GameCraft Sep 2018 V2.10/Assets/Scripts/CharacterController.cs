﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private float dashCooldown;
    [SerializeField] private MovementScript _movementScriptScript;
    [SerializeField] private LayerMask _edibleFish;

    private Stopwatch dashWatch;
    [SerializeField] private bool dashed;

    public float sizeScalar = 1;

    private float ogMovementSpeed;
    private float ogDashSpeed;

    private AudioSource splish;

    void Start()
    {
        splish = GetComponent<AudioSource>();
        sizeScalar = transform.lossyScale.x;
        ogMovementSpeed = _movementScriptScript.speed;
        ogDashSpeed = _movementScriptScript.dashSpeed;

        dashWatch = new Stopwatch();
    }

    private void Update()
    {
        if (dashWatch.Elapsed.TotalSeconds > dashCooldown)
        {
            dashed = false;
            GetComponent<Animator>().SetBool("dash", false);
        }

        sizeScalar = transform.lossyScale.x;

        _movementScriptScript.speed = ogMovementSpeed - (ogMovementSpeed / 4 / sizeScalar);
        _movementScriptScript.dashSpeed = ogDashSpeed;
    }

    public void Dash()
    {
        if (!dashed)
        {
            GetComponent<Animator>().SetBool("dash", true);
            dashed = true;
            dashWatch.Reset();
            dashWatch.Start();

            _movementScriptScript.Dash();
            splish.PlayOneShot(splish.clip);
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (((1 << collider.gameObject.layer) & _edibleFish) != 0)
        {
            float otherSizeScalar = collider.gameObject.GetComponent<CharacterController>().sizeScalar;
            if (!dashed) return;
            if (otherSizeScalar < sizeScalar)
            {
                if (gameObject.tag == "Player" && sizeScalar > 100)
                {
                    var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
                    gameController.Win();
                }

                Destroy(collider.gameObject);
                sizeScalar += otherSizeScalar / 10;

                Vector3 newScale = transform.localScale;
                newScale.x += otherSizeScalar / 10;
                newScale.y += otherSizeScalar / 10;

                transform.localScale = newScale;
            }

            if (otherSizeScalar < sizeScalar && collider.gameObject.tag == "Player")
            {
                var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
                gameController.Lose();
                Destroy(collider.gameObject);
            }
        }
    }
}