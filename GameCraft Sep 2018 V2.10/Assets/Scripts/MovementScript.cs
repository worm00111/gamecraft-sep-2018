﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    public float speed;
    public float dashSpeed;
    [SerializeField] private float rotationSpeed;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void RotateFish(float horizontal, float vertical)
    {
        Vector2 moveVec = new Vector2(vertical, horizontal);

        if (moveVec.sqrMagnitude > 0.1)
        {
            var angle = Mathf.Atan2(moveVec.x, moveVec.y) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, angle)),
                Time.deltaTime * rotationSpeed);
        }
    }

    public void RotateFish(float angle)
    {
        Vector3 newRot = transform.rotation.eulerAngles;
        newRot.z += angle;
        transform.rotation = Quaternion.Euler(newRot);
    }

    public void MoveFish(float horizontal, float vertical)
    {
        Vector2 forceToAdd = new Vector2(horizontal * speed, vertical * speed);
        rb.AddForce(forceToAdd);
    }

    public void Dash()
    {
        rb.AddForce(transform.right * dashSpeed, ForceMode2D.Impulse);
    }
}