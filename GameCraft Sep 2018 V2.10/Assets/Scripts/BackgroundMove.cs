﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove : MonoBehaviour
{

    [SerializeField] private Transform player;
    [SerializeField] private float moveSpeed = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    float xMove = -(player.position.x * moveSpeed);
	    float yMove = -(player.position.y * moveSpeed);

        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(xMove, yMove);
	}
}
