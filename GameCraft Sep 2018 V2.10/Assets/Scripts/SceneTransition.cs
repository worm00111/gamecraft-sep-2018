﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{

//    [SerializeField] private SceneAsset _switchTo;
    [SerializeField] private KeyCode _triggerButton;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(_triggerButton))
	    {
	        SceneManager.LoadScene(1);
	    }
	}
}
