﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSpawnManager : MonoBehaviour
{
    public GameObject[] NPCPrefabs;
    public GameObject player;
    public Camera camera;

    [SerializeField] private float respawnTime;
    [SerializeField] private float _bigFishRareness = 5;

    [SerializeField] private int _maxFish = 30;
    [SerializeField] private float _maxDistance = 20f;
    private float _ogMaxDistance;
    private float ditanceToScreenEdge;

    private CharacterController charController;

    private List<Transform> _currentNpcs;

    private void Start()
    {
        _ogMaxDistance = _maxDistance;
        Vector3 screenEdgeVector = camera.ScreenToWorldPoint(new Vector3(0, 0, 0));
        ditanceToScreenEdge = Vector3.Distance(screenEdgeVector, new Vector3(0, 0, 0));
        _currentNpcs = new List<Transform>();
        charController = player.GetComponent<CharacterController>();
    }

    void Update()
    {
        _maxDistance = (float) (_ogMaxDistance + charController.sizeScalar * 0.6);
        if (player == null) return;

        while (_currentNpcs.Count < _maxFish)
        {
            int randomNPCIndex = Random.Range(0, NPCPrefabs.Length);
            SpawnNPC(NPCPrefabs[randomNPCIndex]);
        }

        RemoveDistantFishes();
    }

    private void RemoveDistantFishes()
    {
        List<Transform> npcsToRemove = new List<Transform>();
        foreach (Transform npc in _currentNpcs)
        {
            if (npc == null)
            {
                npcsToRemove.Add(npc);
                continue;
            }

            float distance = Vector3.Distance(player.transform.position, npc.position);

            if (distance > _maxDistance)
            {
                npcsToRemove.Add(npc);
            }
        }

        foreach (Transform npc in npcsToRemove)
        {
            _currentNpcs.Remove(npc);

            if (npc != null) Destroy(npc.gameObject);
        }
    }

    void SpawnNPC(GameObject typeOfNPC)
    {
        float randomRotation = Random.Range(0, 360);
        float sizeScalar = charController.sizeScalar;
        sizeScalar = sizeScalar +
                     Random.Range(-sizeScalar / 4, sizeScalar / _bigFishRareness);

        Vector2 spawnLocation;

        do
        {
            spawnLocation = Random.insideUnitCircle * _maxDistance;
        } while (Vector3.Distance(spawnLocation, player.transform.position) < ditanceToScreenEdge);

        Vector3 newPosition = spawnLocation;
        Quaternion newRotation = Quaternion.Euler(0, 0, randomRotation);
        GameObject newNPC = (GameObject) (Instantiate(typeOfNPC, newPosition, newRotation));
        _currentNpcs.Add(newNPC.transform);

        CharacterController npcCharController = newNPC.GetComponent<CharacterController>();
        npcCharController.sizeScalar = sizeScalar;
        Vector3 newScale = newNPC.transform.localScale;
        newScale.x = sizeScalar;
        newScale.y = sizeScalar;

        newNPC.transform.localScale = newScale;

        // Add color
        newNPC.GetComponent<SpriteRenderer>().color = Random.ColorHSV();

        newNPC.name = "NPC";
    }
}